# danonicw-issue

Dancing☆Onigiri (CW Edition) に関するIssue用リポジトリです。
本体に関する他、関連ツールについてもここで議論可能です。

## Issue一覧
- [Issues List](https://gitlab.com/cwtickle/danonicw-issue/-/issues)

## Issueの作成方法
1. 左メニューの「Issues」->「List」を選択します。
2. 右上の緑のボタン「New Issue」を選択します。
3. 「要望・見直し」か「不具合報告」により、Titleを設定します。
    - 「要望・見直し」：[Proposed] (ツール名) 要望・見直しの概要
    - 「不具合報告」　：[Bug] (ツール名) 不具合の概要
4. Descriptionより、テンプレートを選択します。
    - 「要望・見直し」：「feature_request」を選択
    - 「不具合報告」　：「bug_report」を選択
5. 記述が必要なテンプレートが表示されるので、それに従って記載し、  
記載が終わったら「Submit Issue」を押します。

## 本家リポジトリ
- [Dancing☆Onigiri (CW Edition)](https://github.com/cwtickle/danoniplus)
- [Dancing☆Onigiri Chart Converter](https://github.com/cwtickle/danoniplus-converter)